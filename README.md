Sample usage:
python3 crawler.py -i input.txt


To halt, press ESC key


Note: Input files are found in the input folder, but there's no need to write
"input/input_name.txt," just "input_name.txt" will do
