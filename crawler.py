import requests
import os
import json
import time
import datetime
import re
import argparse
import sys
import csv
import cProfile
import pprint
from bs4 import BeautifulSoup
from csv import writer
from threading import Thread
from queue import Queue
from pynput.keyboard import Listener, Key


start_time = time.time()
pr = cProfile.Profile()
pr.enable()

# --- Argument Parser -- #
parser = argparse.ArgumentParser(description='Instagram crawler')
parser.add_argument('-i', dest='input_file', type=str, help='Input file. Type -i <name_of_input_file>')
parser.add_argument('--followers', type=int, help='Filter results by number of followers')
parser.add_argument('--following', type=int, help='Filter results by number of following')
parser.add_argument('--posts', type=int, help='Filter results by number of posts')
parser.add_argument('--verified', type=bool, help='Filter results by whether account is verified or not')
parser.add_argument('--private', type=bool, help='Filter results by whether account is private or not')
parser.add_argument('--limit', dest='limit', nargs='?', const=50, type=int, help='Default number of results is 50 when not specified')
# ----

args = parser.parse_args()
users = Queue()
unfiltered = []
filtered = []
exit_na = False
halt = False
limit = 50 if args.limit is None else args.limit

headers = ['ID', 'USERNAME', 'FULL NAME', 'BIOGRAPHY', 'EMAIL', 'VERIFIED', 
    'PRIVATE', 'PROFILE PIC URL', 'FOLLOWERS', 'FOLLOWING', 'POSTS']

print('halting condition is: ', limit)

proxies = {
    'http': 'http://195.24.157.18:80',
    'http': 'http://196.251.14.102:8080',
    'http': 'http://142.93.20.181:3128'
}

# exit_na = False

def get_page(url):
    try:
        page = requests.get(url, proxies=proxies)
        page.raise_for_status()
    except requests.exceptions.HTTPError:
        return None
    except requests.exceptions.ConnectionError:
        return None
    except requests.exceptions.Timeout:
        return None
    except requests.exceptions.RequestException:
        return None
    else:
        return page.text


def get_profile(username):
    global limit
    
    url = 'https://www.instagram.com/' + username + '/'

    try:
        response = get_page(url)
        soup = BeautifulSoup(response, 'lxml')
        general_profile = soup.find('body')
        main_profile = general_profile.find_all('script')
        user_json = main_profile[0].text
        length = len(user_json)
        user_final = user_json[21:length - 1]
        user_j = json.loads(user_final)
        user_profile = user_j["entry_data"]["ProfilePage"][0]["graphql"]["user"]

        user_id = user_profile['id']
        user_un = user_profile['username']
        user_fn = user_profile['full_name']
        user_bio = user_profile['biography']
        emails = re.findall('\S+@\S+', user_bio)
        user_email = (" | ".join(emails))
        user_is_verified = user_profile['is_verified']
        user_is_private = user_profile['is_private']
        user_pp = user_profile['profile_pic_url']
        user_followers = user_profile['edge_followed_by']['count']
        user_following = user_profile['edge_follow']['count']
        user_posts = user_profile['edge_owner_to_timeline_media']['count']

        profile = [user_id, user_un, user_fn, user_bio, user_email, user_is_verified, 
            user_is_private, user_pp, user_followers, user_following, user_posts]

        if filter():
            if user_id not in list(map(lambda x: x[0], unfiltered)):
                unfiltered.append(profile)
            if (len(filtered) < limit) and user_id not in list(map(lambda x: x[0], filtered)):
                filter_results(profile)
        else:
            if (len(unfiltered) < limit) and user_id not in list(map(lambda x: x[0], unfiltered)):
                unfiltered.append(profile)
                print(len(unfiltered))
        
    except (json.decoder.JSONDecodeError, AttributeError, TypeError):
        # return None
        pass

def get_filters():
    global args

    a = vars(args)
    a.pop('input_file', None)
    a.pop('limit', None)
    res = dict((a, b) for (a, b) in a.items() if b is not None)
    return res


def filter():
    return len(get_filters()) > 0


def get_index_filter(name):
    index = 0
    if name == 'followers':
        index = 8
    elif name == 'following':
        index = 9
    elif name == 'posts':
        index = 10
    elif name == 'verified':
        index = 5
    elif name == 'private':
        index = 6
    return index


def filter_results(profile):
    answer = []
    filts = get_filters()
    for k, v in filts.items():
        i = get_index_filter(k)
        if (i >= 8 and int(profile[i]) > v) or (i <= 6 and bool(profile[i]) == v):
            answer.append(True)
        else:
            answer.append(False)
    
    if all(answer):
        filtered.append(profile)
        print('filter: ', len(filtered))


def get_suggested_profiles(username, q):
    url = 'https://www.instagram.com/directory/suggested/' + username + '/'
    html = get_page(url)
    if html is not None:
        soup = BeautifulSoup(html, 'lxml')
        general_profile = soup.find('body')
        main_profile = general_profile.find_all('script')
        script = main_profile[0].text
        length = len(script)
        profile = script[21:length - 1]
        s = json.loads(profile)
        suggested = s["entry_data"]["SuggestedDirectoryPage"][0]["profile_data"]

        for prof in suggested:
            # if (prof not in list(map(lambda x: x[1], unfiltered)) and prof not in list(map(lambda x: x[1], filtered))):
            q.put(prof)


def write_results():
    global headers

    dirr = os.path.dirname(os.path.abspath(__file__))
    dest_dir = os.path.join(dirr, 'unfiltered')
    try:
        os.makedirs(dest_dir)
    except OSError:
        pass

    file_out = 'ig_leads-' + str(datetime.datetime.now().isoformat(' ')) + '.csv'
    path = os.path.join(dest_dir, file_out)
    fwriter = csv.writer(open(path, 'w'))
    fwriter.writerow(headers)
    for p in unfiltered:
        fwriter.writerow(p)

    if filter():
        dest_dir_filtered = os.path.join(dirr, 'filtered')
        try:
            os.makedirs(dest_dir_filtered)
        except OSError:
            pass
        fout = 'ig_leads-' + str(datetime.datetime.now().isoformat(' ')) + '-filtered.csv'
        path_filtered = os.path.join(dest_dir_filtered, fout)
        fw = csv.writer(open(path_filtered, 'w'))
        fw.writerow(headers)
        for r in filtered:
            fw.writerow(r)


class InstagramCrawler(Thread):
    def __init__(self, in_queue):
        Thread.__init__(self)
        self.in_queue = in_queue

    def run(self):
        global exit_na

        while True:
            user = self.in_queue.get()
            is_stop = len(unfiltered) < limit
            # print('stop??? ', is_stop)

            if not halt:
                if(filter()):
                    # print('here?')
                    is_stop = len(filtered) < limit

                if is_stop:
                    # print('still valid')
                    get_data_thread = Thread(target=get_profile, args=[user])
                    # start thread
                    get_data_thread.start()

                    parse_suggested_thread = Thread(target=get_suggested_profiles, args=[user, self.in_queue])
                    parse_suggested_thread.start()
                    parse_suggested_thread.join()
                    get_data_thread.join()
            

            # task for newly enqueued item is complete
            self.in_queue.task_done()

            if filter():
                if (len(filtered) == limit):
                    print('Terminating program...')
                    exit_na = True
                    # return
            else:
                if (len(unfiltered) == limit):
                    print('Terminating program...')
                    exit_na = True
                    # return


def on_release(key):
    global halt
    if key == Key.esc:
        print('Terminating program...')
        halt = True
        return False


def verify_input():
    if (args.input_file is not None):
        try:
            file_in = 'input/' + args.input_file
            with open(file_in) as file:
                for line in file:
                    users.put(line.rstrip())             
                # cProfile.runctx('crawl()', globals(), locals())
        except IOError:
            print("File not found. Try again")
    else:
        print("Input file not provided. Try again")


def main():
    with Listener(on_release=on_release) as listener:
        start_time = time.time()
        verify_input()
        for i in range(5):
            thread = InstagramCrawler(users)
            thread.setDaemon(True)
            thread.start()
        users.join()
        write_results()
        print('finished')
        if exit_na:
            print('Terminated')
            pr.disable()
            pr.print_stats(sort='time')
            print("Finished --- %s seconds ---" % (time.time() - start_time))
            # listener.join()
            # sys.exit()
            return
        listener.join()
    return
    
    # listener = Listener(on_press=None, on_release=on_release)
    # listener.start()


if __name__ == "__main__":
    main()
    print('-----END-----')